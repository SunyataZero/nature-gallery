

# ExifRead
# Version history:
# https://pypi.org/project/ExifRead/#history

#GPSPhoto
# At the time of writing the latest version is 2.1.2
# https://pypi.org/project/GPSPhoto/#history




"""
def get_decimal_degrees_old(i_ifdtag: exifread.IfdTag) -> float:
    # formatted_str_list = i_ifdtag.split(", ")
    # Please note that the space is important since the (arc-) seconds also may have a comma in them

    degrees_int = i_ifdtag.values[0].num / i_ifdtag.values[0].den
    minutes_int = i_ifdtag.values[1].num / i_ifdtag.values[1].den
    seconds_float = i_ifdtag.values[2].num / i_ifdtag.values[2].den

    result_ft = float(degrees_int + minutes_int / 60 + seconds_float / 3600)

    return result_ft



    for tag_key_str in PIL.ExifTags.TAGS.keys():
        if PIL.ExifTags.TAGS[tag_key_str] == "GPSLatitude":
            datetime_original_tag_key_str = tag_key_str
            break

    if "GPSLatitude" in tag_key_str.split():
        latitude_ft = get_decimal_degrees(tag_list[tag_key_str])
        print(latitude_ft)

    elif "GPSLongitude" in tag_key_str.split():
        longitude_ft = get_decimal_degrees(tag_list[tag_key_str])
        # longitude_ft = float(str(tag_list[tag_key_str]))
        print(longitude_ft)


    gps_data = gpsphoto.getGPSData(i_image_pi)
    latitude_ft = gps_data['Latitude']
    longitude_ft = gps_data['Longitude']



    latitude_ft =  -1
    longitude_ft =  -1
    with open(i_file_path, "rb") as file:
        # https://stackoverflow.com/a/18027454/2525237
        tag_list = exifread.process_file(file)
        for tag_key_str in tag_list:
            if "GPSLatitude" in tag_key_str.split():
                latitude_ft = get_decimal_degrees(tag_list[tag_key_str])
                print(latitude_ft)

            elif "GPSLongitude" in tag_key_str.split():
                longitude_ft = get_decimal_degrees(tag_list[tag_key_str])
                # longitude_ft = float(str(tag_list[tag_key_str]))
                print(longitude_ft)
    lat_lng_tuple = (latitude_ft, longitude_ft)


def get_datetime_image_taken(i_image_pi: PIL.Image) -> QtCore.QDateTime:
    # Please note that usually (always?) the time we get from the carmera is in the UTC time zone:
    # https://photo.stackexchange.com/questions/82166/is-it-possible-to-get-the-time-a-photo-was-taken-timezone-aware
    # So we need to convert the time that we get
    ret_datetime_qdt = None
    datetime_original_tag_key_str = ""
    for tag_key_str in PIL.ExifTags.TAGS.keys():
        if PIL.ExifTags.TAGS[tag_key_str] == DATETIME_ORIGINAL_EXIF_TAG_NAME_STR:
            datetime_original_tag_key_str = tag_key_str
            break
    if datetime_original_tag_key_str == "":
        logging.warning("get_datetime_image_taken - exif tag not found")
    try:
        exif_data_dict = dict(i_image_pi._getexif().items())
        # -Good to be aware that _getexif() is an experimental function:
        #  https://stackoverflow.com/a/48428533/2525237
        datetime_exif_string = exif_data_dict[datetime_original_tag_key_str]
        logging.debug("datetime_exif_string = " + datetime_exif_string)
        from_camera_qdt = QtCore.QDateTime.fromString(datetime_exif_string, QT_EXIF_DATETIME_FORMAT_STR)
        from_camera_qdt.setTimeSpec(QtCore.Qt.UTC)
        ret_datetime_qdt = from_camera_qdt.toLocalTime()
        logging.debug("from_camera_qdt.toString = " + ret_datetime_qdt.toString(QT_DATETIME_FORMAT_STR))
    except AttributeError:
        # -A strange problem: If we use hasattr(i_image_pi, "_getexif") this will return True,
        #  so instead we use this exception handling
        logging.warning("get_datetime_image_taken - Image doesn't have exif data. This may be because it has already been processed by an application")

    return ret_datetime_qdt

"""

        """
        with open(image_file_path_str, "rb") as file:
            # https://stackoverflow.com/a/18027454/2525237
            tag_list = exifread.process_file(file)
            i = 1

        latitude_ft = 0
        longitude_ft = 0
        for tag_key_str in tag_list:
            if DATETIME_ORIGINAL_EXIF_TAG_NAME_STR in tag_key_str.split():
                datetime_str = str(tag_list[tag_key_str])
                datetime_pdt = datetime.datetime.strptime(datetime_str, PY_DATETIME_FORMAT_STR)
                month_str = datetime_pdt.strftime("%B")
                print(month_str)

            elif "GPSLatitude" in tag_key_str.split():
                latitude_ft = get_decimal_degrees(tag_list[tag_key_str])
                print(latitude_ft)

            elif "GPSLongitude" in tag_key_str.split():
                longitude_ft = get_decimal_degrees(tag_list[tag_key_str])
                # longitude_ft = float(str(tag_list[tag_key_str]))
                print(longitude_ft)
        """

