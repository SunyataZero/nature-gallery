
Visa platsen, kanske med hjälp av Nominatim:
https://nominatim.openstreetmap.org/reverse?format=xml&lat=57.6582222&lon=12.0370167&zoom=18&addressdetails=1
(exempelbilder finns i "coords-experiment"-katalogen)

Reverse Geocoding:
https://wiki.openstreetmap.org/wiki/Nominatim#Reverse_Geocoding

"Natural" key
https://wiki.openstreetmap.org/wiki/Key:natural

Sverige:
https://wiki.openstreetmap.org/wiki/WikiProject_Sweden/Nature_conservation
https://wiki.openstreetmap.org/wiki/WikiProject_Sweden
